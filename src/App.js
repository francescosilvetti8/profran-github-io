import React, { Component } from 'react';

import MainRouter from './scenes/Router';

import './styles/colors.scss';

class App extends Component {
  render() {
    return (
      <MainRouter/>
    );
  }
}

export default App;
