import React, {Component} from 'react';
import './styles.scss';

class Button extends Component {

  render() {
    return(
      <div className={this.props.primary ? "button primary" : "button"}>{this.props.text}</div>
    );
  }
}

export default Button;
