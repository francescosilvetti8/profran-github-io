import React, {Component} from "react";

import './styles.scss'

class Carousel extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showing: -1,
      images_length: this.props.images.length
    };

    this.nextImage = this.nextImage.bind(this);
  }

  componentDidMount() {
    this.nextImage();
    var intervalId = setInterval(this.nextImage, 3500);
    this.setState({intervalId: intervalId});
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  nextImage() {
    if (this.state.showing !== (this.state.images_length - 1)) {
      this.setState({
        showing: this.state.showing + 1
      });
      console.log(this.state);
    } else {
      this.setState({showing: -1})
    }
  }

  createImages() {

    let build = []

    for (let image in this.props.images) {
      let className = this.state.showing === parseInt(image)
        ? "slide showing"
        : "slide";
      build.push(<div className={className} key={image} style={{
          backgroundImage: "url(" + this.props.images[image] + ")"
        }}></div>)
    }

    return (build);
  }

  render() {
    return (<div className="slides">{this.createImages()}</div>);
  }

}

export default Carousel;
