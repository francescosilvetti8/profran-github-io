import React, {Component} from 'react';
import './styles.scss';

class Footer extends Component {
  render() {
    return(
      <div className="footer">
        @2018 Todos los Derechos Reservados.
      </div>
    );
  }
}

export default Footer;
