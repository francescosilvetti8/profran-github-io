import React, {Component} from 'react';
import { Link } from "react-router-dom";
import './styles.scss';

import Logo from './../../media/logo.svg';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrolling: false
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    if (window.scrollY === 0 && this.state.scrolling === true) {
      this.setState({
        scrolling: false
      });
    } else if (window.scrollY !== 0 && this.state.scrolling !== true) {
      this.setState({
        scrolling: true
      });
    }
  }

  render() {
    return (
    <div className={this.state.scrolling ? "navbar scrolling" : "navbar"}>
      <div className="logo-wrapper">
        <img className="logo" src={Logo} alt=""></img>
      </div>
      <div className="navbar-buttons-wrapper">
        <div className="navbar-button-wrapper navbar-side-button">
          <i className="material-icons">menu</i>
        </div>
        <div className="navbar-button-wrapper"><Link to="/vacualert/">Vacualert</Link></div>
        <div className="navbar-button-wrapper"><Link to="/demo/">Demo</Link></div>
        <div className="navbar-button-wrapper"><Link to="/">Home</Link></div>
      </div>
    </div>);
  }
}

export default Navbar;
