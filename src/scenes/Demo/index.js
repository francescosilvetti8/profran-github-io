import React, {Component} from 'react';
import './styles.scss';

import Navbar from './../../components/Navbar';
import Carousel from './../../components/Carousel';

import Image1 from './../../media/1.PNG';
import Image2 from './../../media/2.PNG';
import Image3 from './../../media/3.PNG';
import Image4 from './../../media/4.PNG';
import Image5 from './../../media/5.PNG';
import Image6 from './../../media/6.PNG';
import Image7 from './../../media/7.PNG';
import Image8 from './../../media/8.PNG';


class Demo extends Component {
  render() {
    return(
      <div className="demo">
        <Navbar/>
        <Carousel images={[Image1, Image2, Image3, Image4, Image5, Image6, Image7, Image8]}/>
      </div>
    );
  }
}

export default Demo;
