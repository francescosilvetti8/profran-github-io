import React, {Component} from 'react';
import './styles.scss';

class Span extends Component {
  render() {
    return(
      <div className="span">
        <i className="icon material-icons">{this.props.icon}</i>
        <h3 className="title">{this.props.title}</h3>
        <p className="desc">{this.props.desc}</p>
      </div>
    );
  }
}

export default Span;
