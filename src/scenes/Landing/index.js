import React, {Component} from 'react';
import './styles.scss';

import Navbar from './../../components/Navbar';
import Button from './../../components/Button';
import Input from './../../components/Input';
import Footer from './../../components/Footer';
import Span from './Components/Span';

import logo_full from './../../media/logo_full.svg';

class Landing extends Component {

  render() {
    return(
      <div>
        <div className="main">
          <Navbar/>
          <h1 className="main-text">Carnet digital de <img className="logo-full" src={logo_full} alt=""/></h1>
          <Button text="Registrate" primary={true}/>
        </div>
        <div className="description">
          <div className="wrapper">
            <h3 className="banner">“El nuevo carnet digital de vacunación al alcance de tu mano, estés donde estés. Somos VacuNación.”</h3>
          </div>
          <div className="wrapper">
            <Span title="Facil" icon="sentiment_very_satisfied" desc="Interfaz facil de usar!"/>
            <Span title="Rapido" icon="thumb_up_alt" desc="Desde la palma de tu mano!"/>
            <Span title="Accesible" icon="money_off" desc="Tenemos tarifas economicas!"/>
          </div>
        </div>
        <div className="contact">
          <h1>Subscribite a las noticias!</h1>
          <form className="subscribe">
            <div className="wrapper-email">
              <Input name="email" type="text" placeholder="E-mail"/>
            </div>
            <Button primary={false} text="Enviar"/>
          </form>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default Landing;
