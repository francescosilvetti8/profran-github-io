import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Landing from './../Landing';
import Demo from './../Demo';
import Vacualert from "../Vacualert";

const MainRouter = () => (
  <Router>
    <div>
      <Route path="/" exact component={Landing} />
      <Route path="/demo/" component={Demo} />
      <Route path="/vacualert" component={Vacualert} />
    </div>
  </Router>
);

export default MainRouter;
