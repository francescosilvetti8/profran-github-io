import React, {Component} from 'react';
import './styles.scss';

import Navbar from './../../components/Navbar';

class Vacualert extends Component {
  render() {
    return(
      <div className="vacualert">
        <Navbar />
        <h1 className="development_text">En desarrollo!</h1>
        <h2><a href="https://www.instagram.com/vacu_alert/">Conoce mas en nuestro <b>Instagram!</b></a></h2>
      </div>
    );
  }
}

export default Vacualert;